# UFR Schiences et Technologies


![decor](assets/images/grille.svg)

Quelques graphiques illustrant les chiffres clés, globalement sur l'UFR et filière par filière :

- pourcentage d'admis.e.s
- équilibre en genre
- niveau de ressource
- taux de réussite par diplôme d'origine
