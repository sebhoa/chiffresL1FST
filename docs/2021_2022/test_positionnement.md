# BAC et spécialités

440 étudiant-es ont répondu à ce premier questionnaire sur les quelques 840 inscrit-es à l'UFR Sciences et Technologies. 

## Quel BAC ?

=== "Néo-Bachelier-e"

    ![neo bachelier](../assets/graphiques/2021/NeoBachelier.svg){: .centrer}

=== "Répartition des anciens BACS"

    ![neo bachelier](../assets/graphiques/2021/BAC_anciens.svg){: .centrer}


## Répartition des spécialités en Terminale

Sans grande surprise, les spécialités les mieux représentées sont les _trois classiques_  occupant les 3/4 des spécialités :

- Mathématiques
- Sciences de la vie et de la terre
- Physique - Chimie

A noter le très bon résultat de la spécialité Numérique et Sciences Informatiques comparé au score National.

![spé en Terminale](../assets/graphiques/2021/SpeTerminale.svg){: .centrer}

On constate que les étudiant-es sont _correctement_  réparti-es dans les filières en fonction de leurs choix de spécialités ; probablement un résultat dû au choix avisé des élèves et la la bonne configuration des coefficients dans _ParcourSup_ :

??? Info "Détail par filière"

    === "Informatique"

        ![spé en Info](../assets/graphiques/2021/spe_info.svg){: .centrer}

    === "Mathématiques"

        ![spé en Maths](../assets/graphiques/2021/spe_maths.svg){: .centrer}

    === "Physique"

        ![spé en Physique](../assets/graphiques/2021/spe_phys.svg){: .centrer}

    === "Chimie"

        ![spé en Chimie](../assets/graphiques/2021/spe_chimie.svg){: .centrer}

    === "Sciences de la Vie"

        ![spé en SV](../assets/graphiques/2021/spe_sv.svg){: .centrer}

    === "Sciences de la Terre"

        ![spé en ST](../assets/graphiques/2021/spe_st.svg){: .centrer}

    === "Sciences de l'Ingénieur-e"

        N'ont pas répondu au sondage.

    === "MIASHS"

        ![spé en MIASHS](../assets/graphiques/2021/spe_miashs.svg){: .centrer}


## Les doublettes

Là aussi sans grande surprise on constate que 76% des choix sont pris par 4 doublettes :

- Physique - chimie / Sciences de la vie et de la terre
- Mathématiques / Physique - chimie
- Mathématiques / Numérique et sciences informatiques
- Mathématiques / Sciences de la vie et de la terre

![spé en Terminale](../assets/graphiques/2021/Doublettes.svg){: .centrer}

Au niveau National on constate l'absence de NSI dans les doublettes :

- Mathématiques / Physique - chimie (20%)
- HGGSP - SES (15%) (cette doublette ne concerne pas vraiment notre UFR)
- Physique - chimie / Sciences de la vie et de la terre (13%)
- Mathématiques / Sciences de la vie et de la terre (7%)


## Spécialités abandonnées

Si on regarde les spécialités avec un nombre d'étudiant-es concerné-es supérieur à 5, les spécialités les plus abandonnées (en % de la présence de cette spécialité en Première) :

- Langues, littératures et cultures étrangère 59%
- Sciences de l’ingénieur 48%
- Mathématiques	40%
- Physique - Chimie	31%
- Sciences de la vie et de la terre 24%
- Sciences économiques et sociales	23%
- Numériques et sciences informatiques 12%


Sauf pour les Mathématiques, les abandons ne suivent pas la tendance Nationale du recul des effectifs dans les filières entre les années 2020 et 2021[^1] :

- Sciences de l’ingénieur	65%
- Littératures, langues et cultures de l’Antiquité	60%
- Numériques et sciences informatiques	54%
- Humanités, littérature et philosophie	48%
- Mathématiques	40%

Toutefois, ces chiffres sont un peu biaisés : la représentation des filières parmi les réponses ne correspond pas tout à fait à la population de la FST.

![répartition par filiere des réponses](../assets/graphiques/2021/participation.svg){: .centrer}

![répartition par filiere de la FST](../assets/graphiques/2021/effectifs.svg){: .centrer}



[^1]: Source : Inforgraphie du [site d'information en ligne letudiant](https://www.letudiant.fr/lycee/specialites-bac-general/article/au-lycee-general-le-choix-des-specialites-n-empeche-pas-les-inegalites.html), données issues du Ministère mai 2021.
