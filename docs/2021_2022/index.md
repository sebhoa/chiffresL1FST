# Tests de positionnement en L1

Accueillant pour la première année des néo-bachelier-es issu-es de la réforme du BAC, l'UFR Sciences et Technologies a décidé de _prendre la témpérature_ :

- quelles spécialités ont été suivies par nos étudiant-es ?
- où en sont-elles, où en sont-ils de la compréhension du français ?
- où en sont-elles, où en sont-ils en éléments de calculs de base (fractions, puissances, logique, calcul littéral etc.) ?