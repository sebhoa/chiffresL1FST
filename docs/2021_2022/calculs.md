# Le test de calculs

Dix questions sur le calcul des fractions, des puissances, de logique, sur du calcul littéral, etc.

## Le test en détail

??? question "Question 1"

    On considère les valeurs suivantes :

    $$\begin{array}{llll}
        A = \frac{28\times 10^3}{0,4\times 10^4} & B = \frac{18\times 10^{-4}}{3\times 10^{-3}} & C = \frac{15\times 10^2}{0,3\times 10^5} & D = \frac{0,7\times 10^5}{14\times 10^3}
    \end{array}$$

    Après simplification on obtient :

    === "Pour A"
        - [ ] 7
        - [ ] 0,7
        - [ ] 0,007
        - [ ] 0,07

    === "Pour B"
        - [ ] 3/5
        - [ ] 0,5
        - [ ] 1/5
        - [ ] 2/5

    === "Pour C"
        - [ ] 0,05
        - [ ] 0,5
        - [ ] 1/5
        - [ ] 5

    === "Pour D"
        - [ ] 5
        - [ ] 0,5
        - [ ] 1/5
        - [ ] 50


??? question "Question 2"

    Sélectionner la ou les affirmations vraies ci-dessous

    - [ ] $\frac{187}{323} = \frac{11}{19}$
    - [ ] $\frac{7}{2} + \frac{1}{3} \gt 4$
    - [ ] $\frac{1}{5} + \frac{1}{2} \lt \frac{3}{4}$
    - [ ] $6\times \frac{3}{4} - 7\times \frac{2}{3} \gt 0$
    - [ ] $\frac{1}{5} + \frac{1}{2} + \frac{1}{3} \gt 1$

??? question "Question 3"

    Soit $a$ un réel,

    - [ ] Si $a\gt -1$, alors $a^2\gt -1$
    - [ ] Si $a\gt -1$, alors $a^2\gt 1$
    - [ ] Si $a\lt -1$, alors $a^2\lt 1$
    - [ ] Si $a\lt -1$, alors $a^2\lt 0$

??? question "Question 4"

    Un père et son fils ont à eux deux 58 ans. Sachant que le père à 28 ans de plus que le fils, quel âge à le père ?

    - [ ] 36 ans
    - [ ] 49 ans
    - [ ] 43 ans
    - [ ] 41 ans
    - [ ] 46 ans

??? question "Question 5"

    On considère les surfaces suivantes :

    ![surfaces](../assets/graphiques/2021/calcul_litteral.png){: .centrer}

    Sélectionner la ou les propositions vraies ci-dessous :

    - [ ] Si $a = 1$, le périmètre du carré jaune est égal au périmètre du rectangle bleu.
    - [ ] Si $a = 2$, l'aire du carré jaune est égale à la somme des aires des 2 surfaces bleues.
    - [ ] Quelque soit la valeur de $a\gt 0$, l'aire du carré jaune est égale à la somme des aires des 2 surfaces bleues.
    - [ ] Quelque soit la valeur de $a\gt 0$, le périmètre du carré jaune vaut la somme des périmètres des surfaces bleues. 

??? question "Question 6"

    Mathilde déclare : "J'ai deux frères de moins que de soeurs". Son frère Luc lui répond : "J'ai deux fois plus de soeurs que de frères."

    Sélectionner la ou les propositions vraies ci-dessous :

    - [ ] La famille comprend cinq garçons.
    - [ ] La famille comprend plus de filles que de garçons.
    - [ ] La famille comprend deux filles.
    - [ ] La famille compte huit enfants.

??? question "Question 7"

    Sélectionner la ou les affirmations vraies ci-dessous :

    - [ ] Si on considère les entiers naturels, $a = 3b + 1\Rightarrow a$ pair.
    - [ ] La proposition $x\geq 0$ et $y\lt 0$ est fausse signifie que $x\lt 0$ et $y\geq 0$.
    - [ ] La formule logique $x\lt 5$ et $x\gt 3$ n'est jamais vraie.
    - [ ] Si on considère les entiers naturels, $n\lt 10$ ou $n\gt 5 \Leftrightarrow n\in \mathbb{N}$
    - [ ] $x\not\in \left[-1, 5\right] \Leftrightarrow x\lt -1$ ou $x\gt 5$.

??? question "Question 8"

    Sélectionner la ou les affirmations vraies ci-dessous :

    - [ ] $0,06kg > 6000mg$
    - [ ] $0,7m^3 = 70dm^3$
    - [ ] $0,125km > 99m$
    - [ ] $5\,425\,351cm^3 = 5,425m^3$
    - [ ] $1mm^3 = 0,01cm^3$

??? question "Question 9"

    Sélectionner la ou les affirmations vraies ci-dessous :

    - [ ] Une citerne cubique d'un mètre de côté contient 1 million de cm<sup>3</sup>
    - [ ] $500min\,68s = 7h\,3min\,8s$
    - [ ] Une citerne cubique d'un mètre de côté contient $1000L$
    - [ ] Une citerne cubique d'un mètre de côté contient $100L$
    - [ ] $500min\,68s = 8h\,21min\,8s$


??? question "Question 10"

    On donne le tableau de variation d'une fonction $f$ continue sur $\mathbb{R}$.

    ![variation](../assets/graphiques/2021/variation.png){: .centrer}

    Quelle(s) proposition(s) est (sont) vraie(s) ?

    - [ ] L'équation $f(x) = 0,5$ a trois solutions.
    - [ ] L'inéquation $f(x)\lt 0$ est vérifiée sur $\mathbb{R}$.
    - [ ] L'inéquation $f(x)\lt 2$ n'a aucune solution.
    - [ ] L'équation $f(x) = 0$ n'a aucune solution.




## Les résultats

Ils sont globalement faibles. Les deux filières les plus faibles sont les L1 Informatique (avec un public très hétérogène) et les L1 Physique. Depuis plusieurs années maintenant cette filière et avant elle la filière IEEA attire de nombreux étudiants issus de bacs technologiques et professionnels, avec un bagage mathématiques quasi inexistant.

![moyenne test de calcul](../assets/graphiques/2021/moy_calcul.svg){: .centrer}

La question 7 est la moins réussie : la moyenne est à 42% et seulement 23% des 517 étudiant-es ont obtenu tous les points. La 1 obtient la meilleure moyenne (à 75%) mais c'est la question 4 à laquelle le plus de monde répond correctement (69% des interrogé-es). Sans surprise, le calcul littéral est le point faible des étudiant-es (questions 5, 6 et 7 sont résolues par à peine 1/4 des personnes) ainsi que la conversion d'unités (question 8).