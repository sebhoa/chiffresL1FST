# Le test de français

Cinq questions portant sur la compréhension de petits mots de logique, de liaison, la compréhension de texte.

## Le test en détail

??? question "Question 1"

    Veuillez compléter le texte ci-dessous avec le mot qui convient (les mots sont tous en minuscules, même si parfois ils sont en début de phrase) :

    _Innombrables sont les cultures dans lesquelles on engage ou clôt la conversation en s'enquérant de la santé de son interlocuteur ou en lui souhaitant de bien se porter. [A] elle se fait de façon un peu mécanique, cette manière d'entrer en contact met bien en évidence l'importance que la santé occupe dans nos existences._ 
    
    _Les pratiques visant à préserver la santé ont toujours existé. Il y a [B] bien des spécificités au souci contemporain de la santé. Celle-ci est [C] devenue un véritable bien de consommation. En témoignent [D] le développement de la presse santé, des « produits-santé », l'essor des thalassothérapies, [E] des denrées anti-cholestérol qui ont pris d'assaut les étalages... Une expression fait désormais florès : « mieux-être »._

    Catherine Halpern, « La santé: à la poursuite d'une utopie », in _Sciences Humaines_, hors série n°48, 2005

    === "Pour A"

        - [ ] où
        - [ ] comme
        - [ ] même si
        - [ ] ou
        - [ ] notamment
        - [ ] par exemple
        - [ ] pourtant

    === "Pour B"

        - [ ] où
        - [ ] comme
        - [ ] même si
        - [ ] ou
        - [ ] notamment
        - [ ] par exemple
        - [ ] pourtant

    === "Pour C"

        - [ ] où
        - [ ] comme
        - [ ] même si
        - [ ] ou
        - [ ] notamment
        - [ ] par exemple
        - [ ] pourtant

    === "Pour D"

        - [ ] où
        - [ ] comme
        - [ ] même si
        - [ ] ou
        - [ ] notamment
        - [ ] par exemple
        - [ ] pourtant

    === "Pour E"

        - [ ] où
        - [ ] comme
        - [ ] même si
        - [ ] ou
        - [ ] notamment
        - [ ] par exemple
        - [ ] pourtant

??? question "Question 2"

    Veuillez compléter le texte ci-dessous avec le mot qui convient (les mots sont tous en minuscules, même si parfois ils sont en début de phrase) :

    _Désormais on ne prend plus forcément rendez-vous pour se faire soigner ou pour prévenir la carie. Se rendre chez le dentiste, cela peut  [A] devenir une obligation cosmétique, [B] la question n'est plus tant de disposer de dents saines que d'être en mesure de les faire admirer._ 

    _[C] les codes vestimentaires paraissent s'assouplir, le corps humain semble, au contraire, cerné par les normes, [D] le suggère la santé florissante de la chirurgie esthétique. Le dernier exemple vient des Etats-Unis, où les ombilicoplasties connaissent beaucoup de succès, [E] parmi les jeunes filles qui recherchent le nombril idéal : petit et vertical. De toute évidence, le clonage humain ne passe pas forcément par des manipulations génétiques._ 

    Jean-Michel Normand, « La dictature du sourire éclatant », _Le Monde_, 15 janvier 2003

    === "Pour A"

        - [ ] ou
        - [ ] pourtant
        - [ ] car
        - [ ] aussi
        - [ ] alors que
        - [ ] comme
        - [ ] en particulier

    === "Pour B"

        - [ ] ou
        - [ ] pourtant
        - [ ] car
        - [ ] aussi
        - [ ] alors que
        - [ ] comme
        - [ ] en particulier

    === "Pour C"

        - [ ] ou
        - [ ] pourtant
        - [ ] car
        - [ ] aussi
        - [ ] alors que
        - [ ] comme
        - [ ] en particulier

    === "Pour D"

        - [ ] ou
        - [ ] pourtant
        - [ ] car
        - [ ] aussi
        - [ ] alors que
        - [ ] comme
        - [ ] en particulier

    === "Pour E"

        - [ ] ou
        - [ ] pourtant
        - [ ] car
        - [ ] aussi
        - [ ] alors que
        - [ ] comme
        - [ ] en particulier

??? question "Question 3"

    Pour chacune des phrases suivantes, choisissez entre **car** et **donc** pour obtenir une phrase logiquement correcte.

    - Les hommes commettent des injustices [A] c'est leur nature.
    - Les jeunes téléspectateurs sont des proies faciles pour les grandes marques, [B] il faut réglementer le passage des spots publicitaires.
    - Les plus grandes découvertes sont dues au hasard, [C] le chercheur doit être attentif et disponible.
    - La compréhension de ce texte est difficile, [D] les liens logiques en sont absents

    === "Pour A"

        - [ ] car
        - [ ] donc

    === "Pour B"

        - [ ] car
        - [ ] donc

    === "Pour C"

        - [ ] car
        - [ ] donc

    === "Pour D"

        - [ ] car
        - [ ] donc

??? question "Question 4"

    Lire le texte et répondre à la question située en-dessous.

    _Depuis la fin du XIIIème siècle, les Français vivent avec une idée de la « modernité » selon laquelle l'innovation technologique et la croissance économique qu'elle permet engendrent le progrès social et le bien-être individuel. Ce postulat est aujourd'hui mis en question. La science est perçue dans son ambivalence : la plupart des avancées significatives ont aussi des conséquences moins favorables pour les utilisateurs et pour l'environnement. Ainsi Internet est l'outil de construction du village global dont rêvaient beaucoup d'utopistes mondialistes ; mais il permet aussi de surveiller la vie privée des internautes et de saboter des systèmes d'information publics ou privés. Le téléphone portable est à la fois un outil de liberté et de dépendance. La voiture est un moyen de transport et d'évasion mais elle tue chaque année des milliers de personnes et pollue l'atmosphère._ 

    Gérard Mermet, _Francoscopie_ 2003, Larousse

    Laquelle de ces propositions vous paraît correspondre **le mieux** au texte ci-dessus ?

    - [ ] Le téléphone portable est dangereux pour la santé.
    - [ ] La science est un outil qu'il faut savoir utiliser.
    - [ ] La science est un danger pour l'humanité.
    - [ ] La science est un bien pour l'humanité.
    - [ ] Il faut se méfier des réseaux sociaux.

??? question "Question 5"

    Lire le texte et répondre à la question située en-dessous.

    _La délinquance routière s'intensifie. Comment résoudre ce problème de santé publique et de politique pénale ? Des spots publicitaires tentent de fixer dans l'esprit des téléspectateurs-conducteurs des images mentales inhibitrices. Des accidents sont montrés qui résultent d'un irrespect des limitations de vitesse. Il s'agit visiblement de rappeler que les règles du code de la route ne sont pas arbitraires: elles sont des moyens en vue de cette fin désirable qu'est la sécurité. Ces démarches ne peuvent qu'échouer. Elles font comme si, dans la conduite routière, le désir dominant était seulement d'aller d'un point à un autre sans dommage. De simples constats et quelques analyses démentent aisément cette « vue de l'esprit ». Tout conspire en fait à masquer ce fait central que la « route » - la route et la conduite automobile-, est un espace social régi par l'affranchissement des « règles normales » de comportement, lesquelles sont caractérisées par le contrôle de soi et par un calcul approximatif d'un rapport effort/bénéfices suffisamment bon. Sur les routes, ces règles sont neutralisées et laissent la place à un espace de non-droit et de violence où la sociabilité principale est celle des hordes: les paisibles citoyens deviennent des meutes d'automobilistes jaloux, agressifs et violents. Socialement la route est une sorte d'état de nature où le chacun-pour-soi rime avec une violence qui n'a pour seule limite que la mort ou la destruction du second corps, la voiture. La route est une piste sauvage où le corps-à-corps est permanent. (...)_

    _Libération_, 16 juillet 2001

    Laquelle de ces propositions vous paraît correspondre le mieux au texte ci-dessus ?

    - [ ] Les spots publicitaires montrant des accidents sont inopérants car les conducteurs n'écoutent pas.
    - [ ] Les campagnes de sécurité routières échouent car elles reposent sur le postulat erroné que sur la route on se conduit en citoyen.
    - [ ] Les gouvernants n'ont pas encore mesuré combien les automobilistes se métamorphosent au volant.
    - [ ] La route est un espace de délinquance où les automobilistes se conduisent comme des voyous.
    - [ ] Les automobilistes conduisent comme des fous malgré le code de la route.


## Les résultats

Bien meilleurs que pour les calculs :

![résultats français](../assets/graphiques/2021/moy_francais.svg){: .centrer}

